# Objectif : apprendre a manipuler les videos
import cv2
import matplotlib.pyplot as plt
import time

# TODO : ouvrir la video avec opencv VideoCapture()
video = cv2.VideoCapture('data/video_drone.mp4')

while True:
    ret, frame = video.read()
    cv2.imshow('', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# TODO : afficher cette video en temps reel (boucle avec cv2.imshow)

while True:
    ret, frame = video.read()
    cv2.putText(frame, " FPS : " + str(fps), (0, 25), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 0), thickness=3)
    cv2.imshow('', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    n_frame += 1

# TODO : ajouter le fps reel a laquelle vous affichez la video lors de l'affichage (cv2.putText)
# TODO : comment pouvez vous accelerer le fps ?