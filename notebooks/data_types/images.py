# Objectif : apprendre a manipuler les images avec opencv
import cv2
import matplotlib.pyplot as plt

# TODO : lire l'image avec opencv (imread) et l'afficher avec pyplot (imshow) pour verifier que tout va bien
img = cv2.imread('data/cat.jpg')
img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
# plt.imsave('./out/cat1.jpg', img)
# plt.imshow(img)
# plt.show()

# TODO : faire une boucle, et changer tout les pixels noirs en blancs
# w = img.shape[0]
# h = img.shape[1]
#
# for y in range(0, h):
#     for x in range(0, w):
#         if max(img[x, y]) < 50:
#             img[x, y] = (255, 255, 255)
#
# plt.imsave('./out/cat2.jpg', img)
# plt.imshow(img)
# plt.show()

# TODO : ecrire le code ci-dessus en plus efficace grace a cv2.floodfill()
cv2.floodFill(img, None, seedPoint=(0, 0), newVal=(255, 255, 255), loDiff=(1, 2, 2, 2), upDiff=(2, 2, 2, 2))
plt.imsave('./out/cat3.jpg', img)
# plt.imshow(img)
# plt.show(img)

# TODO: sauvegarder votre image dans ./out et verifier que celle ci est bien celle que vous voulez
# plt.imsave('./out/cat.jpg', img)

